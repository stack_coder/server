require("dotenv").config();

const express = require('express')

const mongoose = require("mongoose");
const cors = require("cors");
const bodyParser = require("body-parser");
const app = express()


const CreateColRoutes = require("./routes/columns");
const CardsRoutes = require("./routes/cards");


const port = process.env.PORT || 8080;
//DB Connection
mongoose
  .connect(process.env.DATABASE, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
  })
  .then(() => {
    console.log("DB CONNECTED");
  });
  //Middlewares
app.use(bodyParser.json());
app.use(cors());

  app.use("/", CardsRoutes);
  app.use("/",  CreateColRoutes);

app.listen(port,() => {
    console.log("server is running")
})