var mongoose = require("mongoose");

var columnSchema = new mongoose.Schema(
  {

    title: {
      type: String,
      unique:true,
      trim: true
    },
 
    },
  
);


module.exports = mongoose.model("columns", columnSchema );
