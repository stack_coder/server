var mongoose = require("mongoose");
const {ObjectId} = mongoose.Schema

const cardsSchema = new mongoose.Schema({

title:{
        type:String,
        required:true
    },

description:{
    type:String,
    required:true
},

columnid:{
        type:ObjectId,
        ref:"columns"
    }

});
module.exports = mongoose.model("cards", cardsSchema );

