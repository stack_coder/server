var express = require("express");
var router = express.Router();
const { createColumns, getColumns,getColumnsid ,getColumn,columnsbyId,deletecolumns} = require("../controller/columns");
const { cardsbyId} = require("../controller/cards");



router.get("/", getColumns);
router.get("/getcolumns/",getColumnsid)
router.post("/new",createColumns);
router.delete("/deletecol/:columnId",deletecolumns)

router.param("columnId",columnsbyId)
router.param("cardId",cardsbyId)

module.exports = router;
