var express = require("express");
var router = express.Router();
const { createcards, getcards , deletePost,updatePost} = require("../controller/cards");
const { columnsbyId} = require("../controller/columns");



router.get("/cards", getcards);
router.post("/card/new/:columnId",createcards);

router.param("columnId",columnsbyId)

module.exports = router;
