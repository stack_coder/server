const Columns = require("../model/columns");
const Cards = require("../model/cards");
//const {v4 :uuid} = require('uuid');

exports.columnsbyId = ( req,res, next, id) =>{
    Columns.findById(id).exec((err , createcolumn ) =>{
        if( err || !createcolumn){
            return res.status(400).json({
                error:"Columns not found"
            });
        }
        req.profile = createcolumn;
        next();
    })
}

exports.getColumnsid = async (req,res) => {
    try{
        const columns = await Columns.find({})
        console.log({columns})
        res.json(columns)
      
    }catch(err){
        res.status(500).json({ message: err.message})
    }
    } 
    


exports.getColumns = async  (req,res) => {
try{
    const columns = await Columns.find({})
    const cards = await Cards.find({})
    console.log({columns})
    console.log(cards)
    res.json({columns,cards})
  
}catch(err){
    res.status(500).json({ message: err.message})
}
} 


exports.createColumns =  async (req,res) => {  
    const Column = new Columns({
        title: req.body.title
    });
    console.log("CREATING columns",req.body)
     try{
    const newColumn = await Column.save()
    res.status(201).json(newColumn)
     } catch(err){
        res.status(400).json({
            message: err.message
        });
    }
};


exports.deletecolumns = (req,res) => {   
 let columns = req.profile
 columns.remove((err,columns) =>{
 if(err){
     return res.status(400).json({ error:err});
 }
   res.json({ columns})
   })       
}

exports.updatecolumns = (req,res) => {
  User.findByIdAndUpdate(req.body.id,{
        Name:req.body.Name,
        Description:req.body.Description,
        Category:req.body.Category,
    })
    .then(data =>{
        console.log(data)
        res.send("updated")
    })
    .catch(err =>{
        console.log(err)
    })       
 }
 exports.getColumn = async (req,res,next) =>{
     let columns
     try{
         columns =await Columns.findById(req.params.id)
         if(columns == null){
             return res.status(404).json({message:'cannot find columns'})
         }
        }
         catch(err){
return res.status(500).json({ message:err.message})
         }
         res.columns = columns
         next()
     }
 
 "https://cors-anywhere.herokuapp.com/https://react-kanban-server.herokuapp.com/"

 