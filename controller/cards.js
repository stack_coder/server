const Cards = require("../model/cards");

exports.cardsbyId = ( req,res, next, id) =>{
    Cards.findById(id).exec((err , cards ) =>{
        if( err || !cards){
            return res.status(400).json({
                error:"Columns not found"
            });
        }
        req.profile = cards;
        next();
    })
}

exports.getcards = async (req,res) => {
    try{
        const card = await Cards.find()
        
        res.json(card)     
    }catch(err){
        res.status(500).json({ message: err.message})
    }
    }
    
    exports.createcards =  async (req,res) => {  
        const card = new Cards({
            columnid: req.profile,
            title: req.body.title,
            description:req.body.description           
        });
        console.log("CREATING columns",req.body)
        console.log("profile",req.profile)
         try{
        const cards = await card.save()
        res.status(201).json({cards})
         } catch(err){
            res.status(400).json({
                message: err.message
            });
        }
    };